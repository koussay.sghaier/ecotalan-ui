import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { tap } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private baseUrl = 'http://localhost:8081/api/auth/signIn';

  constructor(private http: HttpClient) { }

  
  authenticateUser(email: string, password: string) {
    const body = { username: email, password };
    return this.http.post<any>(`${this.baseUrl}/signIn`, body);
  }

  login(email: string, password: string, rememberMe: boolean): Observable<any> {
    return this.http.post<any>(`${this.baseUrl}/signIn`, { email, password }).pipe(
      tap((response) => {
        // Handle successful login response
        const token = response.jwtToken;

        // Store the token securely based on "Remember Me" preference
        if (rememberMe) {
          localStorage.setItem('access_token', token);
        } else {
          sessionStorage.setItem('access_token', token);
        }
      })
    );
  }
}
