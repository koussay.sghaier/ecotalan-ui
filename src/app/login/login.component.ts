import { Component, OnInit } from '@angular/core';
import { AuthService } from './auth.service';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  focus;
  focus1;
  loginError: string = '';
  rememberMe = false; 
   constructor(private authService: AuthService, private router: Router) { }
  

  ngOnInit() {}
  user = {
    email: '',
    password: ''
  };
  onSubmit(loginForm: NgForm) {
    if (loginForm.valid) {
      const email = this.user.email;
      const password = this.user.password;
      this.authService.login(email, password, this.rememberMe).subscribe(
        (response) => {
          console.log('User logged in successfully!');
          const token = response.jwtToken;
          if (this.rememberMe) {
            localStorage.setItem('access_token', token);
          } else {
            sessionStorage.setItem('access_token', token);
          }
          this.router.navigate(['/dashboard']);
        },
        (error) => {
          console.error('Invalid email or password. Please try again.');
          alert('Invalid email or password. Please try again.');
          this.loginError = 'Invalid email or password. Please try again.';

        }
        );
      } else {
        console.log('Form is invalid. Please check your inputs.');
      }
    }

}
